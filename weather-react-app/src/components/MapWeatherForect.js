import React from 'react';
import * as Api from 'typescript-fetch-api'
import { withRouter } from "react-router";
import moment from 'moment'
import Moment from 'react-moment';

class MapsOFweather extends React.Component {

constructor(props) {
super(props);
const id = this.props.match?.params.id || moment().format('YYYY-MM-DD');
console.log(id);

this.state = {
events: [{id:'id123', date: "2021-04-21", weather: "Sunny", city:"Berlin, Germany"},
{id:'id13', date: "2021-04-21", weather: "Rainy", city:"London, the UK"},
{id:'id13', date: "2021-04-21", weather: "Cold, snow", city:"Oslo, Norway"}
],
date: id
};

this.handleReload = this.handleReload.bind(this);
this.handleReload();
}

async handleReload(event) {
//const response = await api.events({ date: '2021-03-25'/*this.state.targetDate*/ });
//this.setState({ events: response });
}

render() {
return <div>
{/*<button onClick={this.handleReload}>Reload</button> */}
<h2>Maps of weather</h2>
<h3>Hear weather on <Moment format="YYYY/MM/DD">{this.state.date}</Moment> in cities </h3>
<ul>
{this.state.events.map(
(map) =>
<li key={map.id}><Moment format="YYYY/MM/DD">{this.state.date}</Moment>: {map.weather} is in {map.city}</li>)}
</ul>
</div>
}
}